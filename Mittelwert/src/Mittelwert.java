
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
       double a = 28374.0;
       double b = 735.0;
       double mittelwert;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
	   mittelwert = berechneMittelwert(a, b);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
       System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", a, b, mittelwert);
   }
   
   static double berechneMittelwert(double a, double b)
   {
	   return (a + b) / 2.0;
   }

}
